<?php
/* DATOS DE LOS USUARIOS EN JSON
Usuario: angelkrak
Contraseña: angelkrak@123

Usuario: demo
Contraseña: demo
*/
if(!empty($_POST['submit'])) {
	session_name('prueba_de_jwt');
	session_start();
	// Validamos primero que nada el Token CSRF creado al entrar al login
	if(empty($_SESSION['csrf']) || $_SESSION['csrf'] !== $_POST['token_csrf']) {
		header('Location: ../salir.php');
		exit;
	}

	require_once '../class/jwt.php';
	// Estos son datos estaticos pero pueden sustituir por una base de datos...
	$usuarios = file_get_contents('../usuarios.json');

	// Peticiones HTTP
	$usuario = (isset($_POST['usuario'])) ? htmlspecialchars($_POST['usuario']): '';
	$password = (isset($_POST['password'])) ? htmlspecialchars($_POST['password']): '';

	// Encriptamos la contraseña enviada por el formulario con una llave de seguridad privada interna aparte de usar la funcion "password_hash" para mayor seguridad.
	$create_password = base64_encode(hash_hmac('ripemd160', $password, 'change_key_private_', true)); // "true" indica que la salida serán datos binarios sin formato.
	// $create_password = password_hash(base64_encode(hash_hmac('ripemd160', $password, 'change_key_private_', true)), PASSWORD_DEFAULT); // Demo de codificación password

	$userInfo = json_decode($usuarios);
	if(!empty($userInfo)) {
		// Este ciclo no es necesario si usaras una base de datos, solo lo hice para listar mis usuarios locales
		foreach ($userInfo as $userInfo_k => $_userInfo) {
			$usuario_db = $userInfo[$userInfo_k]->usuario;
			$password_db = $userInfo[$userInfo_k]->password;

			if($usuario == $usuario_db && password_verify($create_password, $password_db) === TRUE) {
				$uuid = bin2hex(random_bytes(20)); // Generamos un Identificador seudoaleatorios en bytes para usarlo como referencia en nuestro Token JWT
				$dataJWT = array(
					'header' => [
						'alg' => 'HS256',
						'typ' => 'JWT'
					],
					'payload' => [
						'UUID' => $uuid,
						'permiso' => $userInfo[$userInfo_k]->_userInfo->rango
						// Tu puedes agregar todos los datos que tu quieras, pero recuerda nada sensible ya que "cualquiera" puede descifrar y hasta robar un token JWT
					],
					'secret' => 'mi_llave_secreta' // Cambia esto por una llave privada tuya
				);

				$_SESSION['id'] = intval($userInfo_k)+1;
				$_SESSION['UUID'] = $uuid; // Esto se guarda para hacer comprobaciones entre la sesion "UUID" y el token en la sesion "jwt"
				$_SESSION['jwt'] = $jwt->create($dataJWT)->get();
				$dataJWT = array(
					'token' => $_SESSION['csrf'],
					'secret' => 'inicio',
					'data' => [
						'header' => [
							'alg' => 'HS256',
							'typ' => 'JWT'
						],
						'payload' => [
				     'UUID' => bin2hex(random_bytes(20))
						]
					]
				);
				$actualizar_jwt = $jwt->update($dataJWT)->get(); // Actualizamos el token de CSRF anteriormente creado con un numero de bytes mas grande
				$_SESSION['csrf'] = $actualizar_jwt;
				$_SESSION['_userInfo'] = $userInfo[$userInfo_k]->_userInfo;

				// Regeneramos el ID de la sesion para "evitar" robos o fix, siempre se debe de actualizar todo lo que se genere anteriormente por seguridad.
				session_regenerate_id(true);
				header('Location: ../dashboard/');
				exit;
			}
		}
	}
	echo 'El usuario o contraseña es incorrecto';
	exit;
}