<?php
  session_name('prueba_de_jwt');
  session_start();
  require_once 'class/jwt.php';
  $dataJWT = array(
   'header' => [
     'alg' => 'HS256',
     'typ' => 'JWT'
   ],
   'payload' => [
     'UUID' => bin2hex(random_bytes(6))
     // Aqui podrias poner datos "importantes" sobre el visitante como su IP, user agent, origin, referer, etc...
   ],
   'secret' => 'inicio'
  );
  $crear_csrf = (!empty($_SESSION['csrf'])) ? $_SESSION['csrf'] : $jwt->create($dataJWT)->get();
  if(empty($_SESSION['csrf'])) $_SESSION['csrf'] = $crear_csrf;
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Formulario con JWT</title>
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>
  <link rel="stylesheet" href="./style.css">
</head>
<body>
  <div class="cnt-login">
    <div class="login-wrap">
      <div class="login-html">
        <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
        <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>
        <div class="login-form">
          <div class="sign-in-htm">
            <form action="functions/login.php" method="post" enctype="application/x-www-form-urlencoded">
              <input type="hidden" name="token_csrf" value="<?php echo $crear_csrf; ?>" style="display:none;">
              <div class="group">
                <label for="user" class="label">Usuario</label>
                <input id="user" type="text" name="usuario" class="input">
              </div>
              <div class="group">
                <label for="pass" class="label">Contraseña</label>
                <input id="pass" type="password" name="password" class="input" data-type="password">
              </div>
              <div class="group">
                <input id="check" type="checkbox" class="check" checked>
                <label for="check"><span class="icon"></span> Mantenerme conectado</label>
              </div>
              <div class="group">
                <input type="submit" class="button" name="submit" value="Conectar">
              </div>
              <div class="hr"></div>
              <div class="foot-lnk">
                <a href="#forgot">¿Se te olvidó tu contraseña?</a>
              </div>
            </form>
          </div>
          <div class="sign-up-htm">
            <div class="group">
              <label for="user" class="label">Usuario</label>
              <input id="user" type="text" class="input">
            </div>
            <div class="group">
              <label for="pass" class="label">Contraseña</label>
              <input id="pass" type="password" class="input" data-type="password">
            </div>
            <div class="group">
              <label for="pass" class="label">Repite la contraseña</label>
              <input id="pass" type="password" class="input" data-type="password">
            </div>
            <div class="group">
              <label for="pass" class="label">Correo electrónico</label>
              <input id="pass" type="text" class="input">
            </div>
            <div class="group">
              <input type="submit" class="button" value="Sign Up">
            </div>
            <div class="hr"></div>
            <div class="foot-lnk">
              <label for="tab-1">¿Ya eres miembro?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>