<?php
	session_name('prueba_de_jwt');
	session_start();
	unset($_SESSION, $_COOKIE, $_GLOBALS);
	session_unset();
	session_destroy();
	session_write_close();
	@session_regenerate_id(true);
	$_SESSION = array();
	header('Location: ./');
	exit;
?>